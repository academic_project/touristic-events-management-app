/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testUnitaire;

import exceptionPersonnalise.IntervallFormatException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import dateSearch.Interval;
import dateSearch.IntervalTree;

/**
 * Main bazar qui sert à tester toutes fonctionnalités basiques liées aux arbres
 * d'intervalles, intervalle... Cette classe n'est pas à prendre en compte ...
 * Nous vous l'avons mis pour faire voir que nous testions au fur et à mesure
 * que nous écrivions des nouvelles choses
 *
 * @author loic
 */
public class IntervalTreeMethodsTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*IntervalTree a = new IntervalTree();
        System.out.println(a.equals(IntervalTree.ARBRE_VIDE));
        System.out.println(a.vide());*/

        //Interval[] t = createInterval(10);
//        IntervalTree fg = IntervalTree.ARBRE_VIDE;
//        fg = new IntervalTree(IntervalTree.ARBRE_VIDE, IntervalTree.ARBRE_VIDE, t[2], fg.findDmax(t[2].getHigh()), null);
//        IntervalTree fd = IntervalTree.ARBRE_VIDE;
//        fd = new IntervalTree(IntervalTree.ARBRE_VIDE, IntervalTree.ARBRE_VIDE, t[1], fd.findDmax(t[1].getHigh()), null);
//        IntervalTree test = IntervalTree.ARBRE_VIDE;
//        String[] l= { "ta" , "m", "l", "ta" , "m", "l","ta" , "m", "l", "o" };
//        for (int i = 0; i < 10; i++) {
//            test.insert(t[i],l[i]);
//        }
//    
//        
//        
//
//        System.out.println(test);
        HashMap<String, Integer> t = new HashMap<String, Integer>();
        t.put("", Integer.MIN_VALUE);
        t.put("f", Integer.MIN_VALUE);
        t.put(null, Integer.MIN_VALUE);
        System.out.println(t.get("r"));
    }

    public static final Interval[] createInterval(int nbreInterval) {
        Interval[] res = new Interval[nbreInterval];
        Scanner reader = new Scanner(System.in);
        for (int i = 0; i < nbreInterval; i++) {
            System.out.println("Entrez un borne sroite de lintervalle:");
            int droite = reader.nextInt();
            System.out.println("Entrez borne gauche");
            int gauche = reader.nextInt();
            Interval y = Interval.INTERVALLE_VIDE;
            try {
                y = new Interval(droite, gauche);
            } catch (IntervallFormatException ex) {
                Logger.getLogger(IntervalTreeMethodsTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            res[i] = y;
        }
        return res;
    }
}
