/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testUnitaire;

import exceptionPersonnalise.IntervallFormatException;
import java.util.logging.Level;
import java.util.logging.Logger;
import dateSearch.Interval;

/**
 * Main-bazar qui sert à tester toutes fonctionnalités basiques liées au intervalles ...
 * Cette classe n'est pas à prendre en compte ...
 * Nous vous l'avons mis pour faire voir que nous testions au fur et à mesure
 * que nous écrivions des nouvelles chose
 * @author loic and yacouba 
 */
public class IntervalIntersectTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Interval FixedEvent = new Interval();
        Interval Search = new Interval();
        /*try {
        FixedEvent = new Interval(10, 20);
        Search = new Interval(5, 9);
        } catch (IntervallFormatException ex) {
        Logger.getLogger(IntervalIntersectTest.class.getName()).log(Level.SEVERE, null, ex);
        }*/

        System.out.println(FixedEvent + "************" + Search);   //pour voir si l'instancation a bien fonctionné
        System.out.println(Search.Intersect(FixedEvent));   //censé renvoyer false
        System.out.println( FixedEvent.equals(Interval.INTERVALLE_VIDE));

    }

}
