/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application; //package dédié aux composants et fonctionnalités liés au fonctionnement de l'interface textuelle l'application 

import eventGestion.Event;
import eventGestion.TypesOfEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Consumer;

/**
 * Classe principale de notre projet qui modélise l'interface textuelle de notre
 * application de recherche
 *
 * @author loic and yacouba
 */
public class Appli implements searchMethods {

    public Scanner sc = new Scanner(System.in);
    public VilleDatas ville;

    public Appli() {
        this.ville = new VilleDatas();
    }

    /**
     * Méthode qui sert à lancer le fonctionnement de l'interface textuelle
     *
     * @param e la base de données étant les événements sur-laquelle
     * l'application va se baser
     */
    public void start(HashMap<String, Event> e) {
        this.ville.setEvents(e);
        System.out.println("*** Que faire à Monaco ? ***");
        String rep;
        do {
            askAction();
            System.out.println("Voulez-vous arrêter ou continuer? [n pour stopper]");
            rep = sc.nextLine();
        } while (!(rep.equals("n")));
    }

    /**
     * Méthode qui demande à l'user ce qu'ils veut faire Soit il veut rechercher
     * des événements particuliers Soit il veut voir tous les événements
     */
    private void askAction() {
        String rep;
        do {
            System.out.println("Voulez-vous rechercher des événements ? ou voir tous les événements disponibles? \n\t -\"r\" pour rechercher \n\t -\"v\" pour voir tous les événements");
            rep = sc.nextLine();
        } while (!(rep.equals("r") || rep.equals("v")));
        if (rep.equals("r")) {
            askTypeOfSearch();
        } else {
            showAllEvents();
        }
    }

    /**
     * Méthode qui demande quelle type de recherche il veut effectuer Il a le
     * choix entre 4 types de recherche Voir enum de l'interface searchMethods
     */
    private void askTypeOfSearch() {
        String rep;
        do {
            System.out.println("Quel type de recherche voulez-vous effectuer? (taper exactement un des mots ci-dessous) \n\t -\"TYPE\" pour rechercher selon les types d'événements \n\t -\"DATE\" pour rechercher selon une date \n\t -\"REFERENCE\" pour rechercher selon une référence \n\t -\"DATEetTYPE\" pour rechercher selon un type d'événement et une date ");
            rep = sc.nextLine();
        } while (rep.equals("") || !((Arrays.asList(TypesOfSearch.values())).contains(TypesOfSearch.valueOf(rep))));

        ArrayList<Event> eventFound = search(TypesOfSearch.valueOf(rep)); //On récupère les événements trouvés lors de la recherche
        eventFound.forEach((Event e) -> {
            System.out.println("Evénement trouvé:");
            System.out.println("\t" + e);
        });

        if (eventFound.isEmpty()) {
            System.out.println("Aucun événement trouvé");
        }
    }

    /**
     * Méthode qui affiche tous les événements
     */
    private void showAllEvents() {
        this.ville.getEvents().values().forEach(System.out::println);
    }

    /**
     * Les implémentations des recherches sont dans la classe VilleDatas
     *
     * @param t type de recherche que l'user veut effectuer
     * @return les événements trouvés
     */
    @Override
    public ArrayList<Event> search(TypesOfSearch t) {
        return this.ville.search(t);
    }

    /**
     * Méthode qui appelle un scanner et permet une interface textuelle avec
     * l'user pour lui demander des informations notamment pour les recherches
     * d'événements
     *
     * @param type vrai si on demande à l'user un type d'événement
     * @param date vrai si on demande à l'user une date
     * @param ref vrai si on demande à l'user une référence
     * @return la chaîne de caractères qui représente soit un type, une date,
     * une référence par rapport auxquelles on va effectuer les recherches
     */
    public static String askInfoForSearch(boolean type, boolean date, boolean ref) {
        if (type) {
            return askType();
        } else if (ref) {
            return askRef();
        } else if (date) {
            return askDate();
        } else {
            return null;
        }
    }

    /**
     * Demande à l'user d'entrer deux entiers pour former la date. On vérifie
     * que les entiers sont valides
     *
     * @return la date de la forme [*,*] où * sont les deux nombres
     */
    private static String askDate() {
        int rep1 = 0, rep2 = 0;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Veuillez entrer la date: début (entrée pour valider), puis fin");
            rep1 = sc.nextInt();
            rep2 = sc.nextInt();
        } while (!(rep1 <= rep2 && rep1 >= 1 && rep1 <= 366 && rep2 >= 1 && rep2 <= 366));
        return "[" + String.valueOf(rep1) + "," + String.valueOf(rep2) + "]";
    }

    /**
     * Demande à l'user d'entrer une référence d'événement
     *
     * @return la référence
     */
    private static String askRef() {
        String rep = "";
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Veuillez entrer une référence : (de la forme)\n\ttitre_de_l'événement(en minuscules sans espace ni accent).jour_début-jour_fin.type_de_l'événement (ignorer les informations en plus de ces trois!!)");
            rep = sc.nextLine();
        } while (!(rep.matches("[a-z]+\\.([1-9]\\d?|[12]\\d{2}|3[0-5]\\d|36[0-5])\\-([1-9]\\d?|[12]\\d{2}|3[0-5]\\d|36[0-5])\\.(Théâtre|Exposition|Sport|Salon|Conférence)")));
        return rep;
    }

    /**
     * Demande à l'user un type d'événement
     *
     * @return le type
     */
    private static String askType() {
        String rep = "";
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Veuillez entrer un type d'événement parmi (taper exactement un des mots ci-dessous) :\n\t-THEATRE\n\t-EXPOSITION\n\t-SPORT\n\t-SALON\n\t-CONFERENCE");
            rep = sc.nextLine();
        } while (!((Arrays.asList(TypesOfEvent.values())).contains(TypesOfEvent.valueOf(rep))) || rep.equals(""));
        return rep;
    }

}
