/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import dateSearch.IntervalTree;
import eventGestion.Event;
import eventGestion.TypesOfEvent;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * classe qui modélise une ville avec ses événements et les recherches possibles
 * sur ces événements
 *
 * @author loic and yacouba
 */
public class VilleDatas implements searchMethods {

    private HashMap<String, Event> events;         //Contient en clès les références et en valeurs les événements
    private String ville;
    private final IntervalTree tree;               //l'arbre d'intervalles en cohérence avec les événements présents dans le hashmap

    public VilleDatas() {
        events = new HashMap<>();
        ville = "Monaco";
        tree = new IntervalTree();
    }

    public HashMap<String, Event> getEvents() {
        return events;
    }

    public void addEvents(Event... event) {
        for (Event e : event) {
            this.events.put(e.getReference(), e);
        }
    }

    public void setEvents(HashMap<String, Event> events) {
        this.events = events;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public IntervalTree getTree() {
        return tree;
    }

    /**
     * Selon un type de recherche, on va appliquer une recherche sur le hashmap
     * des événements Méthode implenté de l'interface searchMethods
     *
     * @param t type de recherche voulu par l'utilisateur
     * @return l'ensemble des événements trouvés
     */
    @Override
    public ArrayList<Event> search(TypesOfSearch t) {
        switch (t) {
            case TYPE:
                return searchType(TypesOfEvent.valueOf(Appli.askInfoForSearch(true, false, false)));
            case DATE:
                return matchEventsFromRefs(searchDate(Appli.askInfoForSearch(false, true, false)));
            case DATEetTYPE:
                ArrayList<Event> res = searchType(TypesOfEvent.valueOf(Appli.askInfoForSearch(true, false, false)));
                res.retainAll(matchEventsFromRefs(searchDate(Appli.askInfoForSearch(false, true, false))));
                return res;
            case REFERENCE:
                return searchRef(Appli.askInfoForSearch(false, false, true));
            default:
                return null;
        }
    }

    /**
     * On recherche les événements dans le hashmap selon un type.
     * 
     * La particularité est qu'il faut split les clés (donc les références) de
     * this.events selon "." et prendre la 3ème partie qui correspond au type de
     * l'événement.
     *
     * @param t le type d'événements selon lequel on cherche
     * @return l'ensemble des événements correspondants au type passé en
     * paramétre, trouvés dans le hashmap
     */
    private ArrayList<Event> searchType(TypesOfEvent t) {
        var res = new ArrayList<Event>();
        this.events.keySet().stream().filter((next) -> (next.split("\\.")[2].equals(t.getName()))).forEachOrdered((next) -> {
            res.add(this.events.get(next));
        });
        return res;
    }

    /**
     * Comme on sait que dans la méthode Appli#askRef() l'user rentre une
     * référence avec seulement les trois premières informations ici on parcourt
     * les clés (qui sont les références) dont on extrait les trois premières
     * informations et on compare
     *
     * @param ref la référence que l'user a donné composé de trois parties
     * d'informations. C'est elle qu'on va comparer avec les clés
     * @return les événements trouvés
     */
    private ArrayList<Event> searchRef(String ref) {
        ArrayList<Event> res = new ArrayList<>();
        this.events.keySet().forEach((String reff) -> {
            String[] t = (reff.split("\\."));
            String c = String.join(".", t[0], t[1], t[2]);  //on a extrait les trois premières parties de la référence reff
            if (c.equals(ref)) {
                res.add(this.events.get(reff));
            }
        });
        return res;
    }

    /**
     * La recherche est délégué ici à l'attribut tree plus précisemment à la
     * méthode IntervalTree#searchEvents()
     *
     * @param date la date que l'user a rentré
     * @return les références associés aux dates trouvées dans l'arbre
     */
    private ArrayList<String> searchDate(String date) {
        this.tree.insertFromEventsCollection(this.events.values());
        return this.tree.searchEvents(date);
    }

    /**
     * Ici on fait le lien entre l'ensemble des références renvoyès par
     * searchDate() et le hashmap this.events, afin de renvoyer un ensemble
     * d'événements. On regarde si chaque référence est une clé de notre hashmap
     *
     * @param ref le tableau de références renvoyé par la recherche dans l'arbre
     * d'intervalles
     * @return les événements trouvés
     */
    private ArrayList<Event> matchEventsFromRefs(ArrayList<String> ref) {
        ArrayList<Event> res = new ArrayList<>();
        ref.stream().filter((String s) -> {
            return !(this.events.get(s) == null);      //this.events.get(s) renvoie null si aucune clé (ici notre référence) s est associé à une valeur (ici un événement)
        }).forEachOrdered((String s) -> {  
            res.add(this.events.get(s));
        });
        return res;
    }
}
