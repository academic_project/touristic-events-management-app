/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import eventGestion.*;
import exceptionPersonnalise.DateFormatException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * main qui lance l'application
 *
 * @author loic and yacouba
 */
public class AppliDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Appli whatsOn = new Appli(); // on crée une appli

        HashMap<String, Event> events = buildDataEvents(); //on stocke les événements

        whatsOn.start(events); // et on lance l'appli sur les données ci-dessus
    }

    //Les deux méthodes représente les deux point qui restent à améliorer dans ce projet
    //La partie administration: si on veut ajouter un événement il faut l'écrire en-dessous dans la méthode; ce n'est pas très pratique !!
    //La partie date: faire le lien entre une date et le jour de l'année sachant que la méthode que nous avons écris ne fonctionne que pour des dates de l'année 2020
    
    
    /**
     * On crée un Hashmap qui contiendra les événements qui vont constituer
     * notre base de données
     *
     * @return le hashmap contenant nos événements
     */
    public static HashMap<String, Event> buildDataEvents() {
        HashMap<String, Event> events = new HashMap<>();
        try {
            Event e1 = new SportEvent("basket", "[" + dateToDayyear(11, 4) + "," + dateToDayyear(11, 4) + "]");
            events.put(e1.getReference(), e1);
            Event e2 = new SportEvent("GrandprixHisto", "[" + dateToDayyear(8, 5) + "," + dateToDayyear(10, 5) + "]");
            events.put(e2.getReference(), e2);
            Event e3 = new SportEvent("Grandprix-F1", "[" + dateToDayyear(21, 5) + "," + dateToDayyear(24, 5) + "]");
            events.put(e3.getReference(), e3);
            Event e4 = new SportEvent("challengeInterBanques", "[" + dateToDayyear(13, 6) + "," + dateToDayyear(14, 6) + "]");
            events.put(e4.getReference(), e4);
            Event e5 = new ExpoEvent("disonaures", "[" + dateToDayyear(11, 4) + "," + dateToDayyear(13, 4) + "]", false);
            events.put(e5.getReference(), e5);
            Event e6 = new ExpoEvent("forumArtistes", "[" + dateToDayyear(6, 6) + "," + dateToDayyear(14, 6) + "]", false);
            events.put(e6.getReference(), e6);
            Event e7 = new SalonEvent("MCFashionWeek", "[" + dateToDayyear(14, 5) + "," + dateToDayyear(16, 5) + "]");
            events.put(e7.getReference(), e7);
            Event e8 = new SalonEvent("TopMarquesMC", "[" + dateToDayyear(11, 6) + "," + dateToDayyear(14, 6) + "]");
            events.put(e8.getReference(), e8);
            Event e9 = new ConfEvent("leCorailEtSesSecrets", "[" + dateToDayyear(27, 4) + "," + dateToDayyear(27, 4) + "]");
            events.put(e9.getReference(), e9);
            Event e10 = new TheatreEvent("diableDhomme", "[" + dateToDayyear(23, 4) + "," + dateToDayyear(24, 4) + "]");
            events.put(e10.getReference(), e10);
        } catch (DateFormatException ex) {
            Logger.getLogger(AppliDriver.class.getName()).log(Level.SEVERE, null, ex);
        }
        return events;
    }

    /**
     * Notre application est limité à l'année 2020 en terme de recherche
     * temporelle. Ainsi cette méthode retourne le jour n-ième de l'année 2020
     * correspondant aux valeurs des paramètres
     *
     * @param jour le jour du mois (1 à 31)
     * @param mois la valeur numérique du mois (1 à 12)
     * @return le jour de l'année correspondant (1 à 366)
     */
    public static String dateToDayyear(int jour, int mois) {
        HashMap<Integer, Integer> y = new HashMap<>();
        y.put(1, 31);
        y.put(2, 29);
        y.put(3, 31);
        y.put(5, 31);
        y.put(7, 31);
        y.put(8, 31);
        y.put(10, 31);
        y.put(12, 31);
        y.put(4, 30);
        y.put(6, 30);
        y.put(9, 30);
        y.put(11, 30);
        int res = 0;
        if (mois != 1) {
            for (int i = 1; i <= mois - 1; i++) {
                res = res + y.get(i);
            }
        }
        res += jour;
        return Integer.toString(res);
    }
}
