/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import eventGestion.Event;
import java.util.ArrayList;

/**
 * interface qui sera par toutes classe qui gère les recherches
 *
 * @author loic and yacouba
 */
public interface searchMethods {

    /**
     * enum qui sert toutes les possibilités de recherche
     */
    public enum TypesOfSearch {
        TYPE, DATE, REFERENCE, DATEetTYPE;
    }

    /**
     * méthode qui retourne les événements trouvés lors de la recherche
     *
     * @param t la type de recherche qu'on va lancer
     * @return les événements trouvés
     */
    public ArrayList<Event> search(TypesOfSearch t);
}
