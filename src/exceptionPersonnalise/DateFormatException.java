/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptionPersonnalise;

/**
 * Cette exception sert notamment dans le constructeur d'un objet Event. Créer
 * une exception s'est avéré important car la date associée à un événement ne
 * peut pas être imcomplète ou erronée...
 *
 * @author loic and yacouba
 */
public class DateFormatException extends Exception {

    public DateFormatException(String date) {
        System.out.println("Vous essayez d'instancier un événement avec le mauvais format de date");
        System.out.println("\t-> " + date);
    }

}
