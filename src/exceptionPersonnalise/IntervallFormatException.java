/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptionPersonnalise;

/**
 * Cette exception sert notamment dans le constructeur d'un objet Interval.
 *
 * @author loic and yacouba
 */
public class IntervallFormatException extends Exception {

    public IntervallFormatException(int low, int high) {
        System.out.println("Vous essayez d'instancier un intervalle avec des valeurs de borne incohérentes");
        System.out.println("\t-> " + low + " et " + high);
    }
}
