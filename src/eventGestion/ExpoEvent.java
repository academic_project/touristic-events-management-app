/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventGestion;

import exceptionPersonnalise.DateFormatException;

/**
 * Sous-classe associée aux événements de type exposition
 *
 * @author loic and yacouba
 */
public class ExpoEvent extends Event {

    public final static TypesOfEvent TYPE = TypesOfEvent.EXPOSITION;
    private boolean guided;

    public ExpoEvent(String title, String date, String place, boolean guided) throws DateFormatException {
        super(title, date, place);
        this.guided = guided;
    }

    public ExpoEvent(String title, String date, boolean guided) throws DateFormatException {
        super(title, date);
        this.guided = guided;
    }

    @Override
    public String createReference() {
        String res = super.createReference();
        return res + "." + TYPE.getName() + "." + (guided ? "Guidè" : "non-Guidé");
    }
}
