/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventGestion;

/**
 * Enum qui contient les constantes correspondant aux différents types d'événements possibles
 * @author loic and yacouba
 */
public enum TypesOfEvent {
    SPORT("Sport","sp"), EXPOSITION("Exposition","e"), SALON("Salon","s"), FOIRE("Foire","f"), CONFERENCE("Conférence","c"), THEATRE("Théâtre","t");
    
    private final String name,raccourci;
    
    TypesOfEvent(String s, String r){
        name=s;
        raccourci=r;
    }
    
    public String getName() {
        return name;
    }

    public String getRaccourci() {
        return raccourci;
    }
    
    
}
