/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventGestion;

import exceptionPersonnalise.DateFormatException;

/** 
 * Sous-classe associée aux événements de type conférence 
 * 
 * @author loic and yacouba
 */
public class ConfEvent extends Event{
    
    public final static TypesOfEvent TYPE=TypesOfEvent.CONFERENCE;

    public ConfEvent(String title, String date) throws DateFormatException {
        super(title, date);
    }

    public ConfEvent(String title, String date, String place) throws DateFormatException {
        super(title, date, place);
    }
    
    @Override
    public String createReference(){
        String res=super.createReference();
        return res+"."+TYPE.getName();
    }
}
