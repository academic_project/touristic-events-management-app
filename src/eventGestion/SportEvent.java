/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventGestion;

import exceptionPersonnalise.DateFormatException;

/**
 * Sous-classe associée aux événements de type sport
 *
 * @author loic and yacouba
 */
public class SportEvent extends Event {

    public final static TypesOfEvent TYPE = TypesOfEvent.SPORT;

    public SportEvent(String title, String date) throws DateFormatException {
        super(title, date);
    }

    public SportEvent(String title, String place, String date) throws DateFormatException {
        super(title, place, date);
    }

    @Override
    public String createReference() {
        String res = super.createReference();
        return res + "." + TYPE.getName();
    }
}
