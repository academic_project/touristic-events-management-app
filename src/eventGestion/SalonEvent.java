/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventGestion;

import exceptionPersonnalise.DateFormatException;

/**
 * Sous-classe associée aux événements de type salon/foire
 *
 * @author loic and yacouba
 */
public class SalonEvent extends Event {

    public final static TypesOfEvent TYPE = TypesOfEvent.SALON;
    private boolean publique, gratuit;

    public SalonEvent(String title, String date) throws DateFormatException {
        super(title, date);
    }

    public SalonEvent(boolean publique, boolean gratuit, String title, String date, String place) throws DateFormatException {
        super(title, date, place);
        this.publique = publique;
        this.gratuit = gratuit;
        this.reference = this.createReference();
    }

    @Override
    public String createReference() {
        String res = super.createReference();
        return res + "." + TYPE.getName() + "." + (publique ? "Ouvert_tous_publique" : "Privé") + "." + (gratuit ? "Gratuit" : "Payant");
    }
}
