/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventGestion;

import exceptionPersonnalise.DateFormatException; //Pour vérifier le format de la date lors de la construction d'un objet Event

/**
 * https://stackoverflow.com/questions/4395811/regex-for-value-range-from-1-365
 * pour la regex du constructeur 
 * A noter pour les regex que en java, pour
 * échapper un caractère, il faut double \\
 *
 * La classe-mère des événements (de Monaco durant l'année 2020)
 *
 * @author loic and yacouba
 */
abstract public class Event {

    /**
     * un événement est caractérisé pour un lieu, un titre, et une date (dans
     * les sous-classes on en ajoutera peut-être d'autres selon le type
     * d'événements) on suppose que l'attribut date est de la forme "[d,f]"
     */
    protected String title, place, date, reference;

    /**
     * Constructeur qui instancie un événement avec toutes les informations ;
     * dont on vérifie la validité de la date passé en paramètre
     *
     * @param title Intitulé de l'événement
     * @param date Date de l'événement
     * @param place Lieu de l'événement
     * @throws DateFormatException si la date passée en paramètre n'est pas un
     * String de la forme "[*,*]" où * doit être un entier entre 1 et 365
     */
    public Event(String title, String date, String place) throws DateFormatException {
        String[] n = date.replaceAll("(\\[|\\])", "").split(",");

        // si l'argument n'est pas de la forme "[*,*]" où * doit être un entier entre 1 et 365 OU que les entiers passés sont pas croissants entre eux
        if (!(date.matches("\\[([1-9]\\d?|[12]\\d{2}|3[0-5]\\d|36[0-5]),([1-9]\\d?|[12]\\d{2}|3[0-5]\\d|36[0-5])\\]")) || Integer.parseInt(n[0]) > Integer.parseInt(n[1])) {
            throw new DateFormatException(date);
        } else {
            this.title = title;
            this.date = date;
            this.place = place;
            this.reference = this.createReference();
        }
    }

    /**
     * Constructeur qui instancie un objet événement dont on ne sait pas le lieu
     *
     * @param title Intitulé de l'événement
     * @param date Date de l'événement
     * @throws DateFormatException si la date passée en paramètre n'est pas un
     * String de la forme "[*,*]" où * doit être un entier entre 1 et 365
     */
    public Event(String title, String date) throws DateFormatException {
        this(title, date, null);
    }

    /**
     * Construit l'événement null (vide, non-défini ...)
     */
    public Event() {
        this.date = null;
        this.place = null;
        this.title = null;
    }

    /**
     * créer la référence d'un événement, de la forme "title.D-F" où title est
     * le titre en minuscule et D début F fin de l'événement nous n'ajoutons pas
     * d'informations sur le lieu dans la référence car un événement ne possède
     * pas obligatoirement un lieu
     *
     * @return la référence d'un événement
     */
    public String createReference() {
        if (this == null) {
            return null;
        } else {
            return this.title.toLowerCase() + "." + this.date.replaceAll("(\\[|\\])", "").replace(",", "-");
        }
    }

    @Override
    public String toString() {
        return "Evénement : " + title + ", se déroule à " + (place == null ? "(lieu inconnu)" : place) + ", pendant la période " + date + ", de référence " + reference;
    }

    /**
     * Getter de l'attribut date
     *
     * @return la date d'un événement [*,*] "[*,*]" où * doit être un entier
     * entre 1 et 365 et croissants entre eux
     */
    public String getDate() {
        return date;
    }

    /**
     * Getter de l'attribut reference
     *
     * @return la référence associé à l'événement
     */
    public String getReference() {
        return reference;
    }

    /**
     * On remplace la caractère "[" ou "]" par la chaîne vide "" et ensuite on
     * split via le caractère ","
     *
     * @return la première composante numérique de la date de l'événement; le
     * jour où commence l'événement
     */
    public int getDebut() {
        String t = this.date.replaceAll("(\\[|\\])", "").split(",")[0];
        return Integer.parseInt(t);
    }

    /**
     * On remplace la caractère "[" ou "]" par la chaîne vide "" et ensuite on
     * split via le caractère ","
     *
     * @return la deuxième composante numérique de la date de l'événement; le
     * jour où commence l'événement
     */
    public int getFin() {
        String t = this.date.replaceAll("(\\[|\\])", "").split(",")[1];
        return Integer.parseInt(t);
    }
}
