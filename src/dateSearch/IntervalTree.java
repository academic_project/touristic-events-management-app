/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dateSearch;

import eventGestion.Event;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * Classe qui modélise un arbre d'intervalles
 *
 * @author loic and yacouba et on s'est inspiré pour le toString():
 * http://www-sop.inria.fr/oasis/personnel/Guillaume.Dufay/MASS2/tp11/Arbre.java
 */
public class IntervalTree {

    private IntervalTree gauche, droite;
    protected Interval node;
    private Integer dmax;
    private ArrayList<String> eventAssociated;

    public static final IntervalTree ARBRE_VIDE = new IntervalTree();

    private IntervalTree(IntervalTree gauche, IntervalTree droite, Interval node, Integer dmax, ArrayList<String> refFound) {
        this.gauche = gauche;
        this.droite = droite;
        this.node = node;
        this.dmax = dmax;
        this.eventAssociated = refFound;
    }

    /**
     * Créer un arbre d'intervalle avec une feuille qui est l'intervalle i
     *
     * @param i intervalle qui sera donc la racine
     */
    public IntervalTree(Interval i) {
        this(null, null, i, null, null);
    }

    /**
     * Créer l'arbre vide où on pourra ajouter par la suite des éléments
     */
    public IntervalTree() {
        this(null, null, new Interval(), null, null);
    }

    /**
     * vérifie si un arbre est vide
     *
     * @return vrai si l'arbre courant est vide, faux sinon
     */
    public boolean vide() {
        return node.equals(Interval.INTERVALLE_VIDE) && gauche == null && droite == null && dmax == null && eventAssociated == null;
    }

    /**
     * Insére un intervalle et une référence à l'arbre courant. Le
     * positionnement est dicté par la borne gauche de l'intervalle Le cas de
     * l'égalité entre les bornes gauches de l'intervalle courant et
     * l'intervalle à insérer, sera ajouté à gauche
     *
     * Note importante: this doit être la racine (l'élément le plus haut) de
     * l'arbre auquel on ajoute l'élément pour que l'appel de
     * updateDmaxForTree() traite bien tous les éléments de l'arbre
     *
     * @param i intervalle à insérer
     * @param ref une fois la place dans l'arbre on ajoute la référence en
     * parallèle de l'intervalle
     */
    public void insert(Interval i, String ref) {
        if (this.vide()) {
            node = i;
            this.eventAssociated = new ArrayList<>();
            this.eventAssociated.add(ref);
        } else if (i.getLow() <= node.getLow()) {
            if (gauche == null) {
                gauche = new IntervalTree(i);
                gauche.eventAssociated = new ArrayList<>();
                gauche.eventAssociated.add(ref);
            } else {
                gauche.insert(i, ref);
            }
        } else {
            if (droite == null) {
                droite = new IntervalTree(i);
                droite.eventAssociated = new ArrayList<>();
                droite.eventAssociated.add(ref);
            } else {
                droite.insert(i, ref);
            }
        }
        //à ce niveau là de la récursion de notre méthode, this est notre racine de l'arbre donc ici on met à jour les dmax de tous les éléments ajoutés ci-avant
        this.updateDmaxForTree();
    }

    /**
     * Créer une arbre d'intervalle à partir d'un ensemble d'événements
     *
     * @param e une collection (dans notre cas une ArrayList) dont les éléments
     * sont des objets Events ou ses sous-types
     */
    public void insertFromEventsCollection(Collection<? extends Event> e) {
        e.forEach((Event next) -> {
            IntervalTree.this.insert(Interval.EventToInterval(next), next.getReference());
        });
    }

    /**
     * Une fois les éléments ( intervalles, références ...) on met à jour tous
     * les dmax de chaque noeud. On part de la racine, on attribue la valeur de
     * dmax pour ce noeud via l'appel findDmax() et ensuite, on effectue cette
     * action pour le sous-arbre gauche et droit.
     *
     * Ainsi, this doit impérativement être la racine (l'élément le plus haut)
     * de l'arbre courant pour que la récursion s'applique sur tous les éléments
     */
    private void updateDmaxForTree() {
        this.setDmax(this.findDmax(this.getNode().getHigh()));
        if (!(droite == null)) {
            droite.updateDmaxForTree();
        }
        if (!(gauche == null)) {
            gauche.updateDmaxForTree();
        }
    }

    /**
     * Trouve la valeur à attribuer au noeud courant this en parcourant ainsi
     * tous ses sous-arbres (droit ET gauche) et comparant avec un entier (le
     * max des bornes droites) trouvé au-dessus dans la hiérarchie de l'arbre
     *
     * @param i théoriquement la borne droite du noeud this, mais au fur et à
     * mesure de la récursion, cet entier change si une plus grande borne droite
     * est rencontrée
     *
     * @return le dmax à associer au noeud courant this
     */
    private Integer findDmax(int i) {
        //si il n'y a pas de sous-arbres dans lesquels aller chercher
        if (droite == null && gauche == null) {
            return i;
        } else {
            int maxDroite = 0; // variable qui contiendra le maximum des bornes droites rencontré dans le sous-arbre droit de this
            int maxGauche = 0;// variable qui contiendra le maximum des bornes droites rencontré dans le sous-arbre gauche de this

            if (!(droite == null)) {
                maxDroite = droite.node.getHigh();
                if (Math.max(i, maxDroite) == maxDroite) {      //si on a rencontré un niveau maximum poosible pour dmax
                    maxDroite = droite.findDmax(maxDroite);
                } else {
                    maxDroite = droite.findDmax(i);             //sinon on continue à comparer par rapport au dmax de la racine
                }
            }

            if (!(gauche == null)) {
                maxGauche = gauche.node.getHigh();
                if (Math.max(i, maxGauche) == maxGauche) {
                    maxGauche = gauche.findDmax(maxGauche);
                } else {
                    maxGauche = gauche.findDmax(i);
                }
            }
            return Math.max(i, Math.max(maxDroite, maxGauche));     //on retourne le max entre les trois valeurs possibles de dmax 
        }
    }

    /**
     * Renvoie l'intervalle associé à this
     *
     * @return un intervalle
     */
    public Interval getNode() {
        return node;
    }

    /**
     * Modifie la valeur de dmax de this
     *
     * @param dmax
     */
    public void setDmax(int dmax) {
        this.dmax = dmax;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IntervalTree other = (IntervalTree) obj;
        if (!Objects.equals(this.dmax, other.dmax)) {
            return false;
        }
        if (!Objects.equals(this.gauche, other.gauche)) {
            return false;
        }
        if (!Objects.equals(this.droite, other.droite)) {
            return false;
        }
        if (!Objects.equals(this.node, other.node)) {
            return false;
        }
        return Objects.equals(this.eventAssociated, other.eventAssociated);
    }

    @Override
    public String toString() {
        if (vide()) {
            return "Arbre vide";
        }
        return toString("\t");
    }

    /**
     * méthode qui permet de faire une récrusion car présence d'un paramètre à
     * chaque récursion on rajoute une tabulation. Donc deux éléments au même
     * niveau de tabulation sont au même étage au niveau de la hiérarchie de
     * l'arbre. C'est pour ça que j'aime bien cette version de toString(): elle
     * est assez intuitive
     *
     * @param s nombre de tabulations selon l'étage (en gros...)
     * @return la valeur à afficher par la méthode publique toString ()
     */
    private String toString(String s) {
        if (!(gauche == null)) {
            if (!(droite == null)) {
                return (s + node.toString() + " dmax= " + dmax + "\n" + gauche.toString(s + "\t") + droite.toString(s + "\t"));
            } else {
                return (s + node.toString() + " dmax= " + dmax + "\n" + gauche.toString(s + "\t") + "\n");
            }
        } else {
            if (!(droite == null)) {
                return (s + node.toString() + " dmax= " + dmax + "\n\n" + droite.toString(s + "\t"));
            } else {
                return (s + node.toString() + " dmax= " + dmax + "\n");
            }
        }
    }

    /**
     * Ce pourquoi a été créée cette classe !!! Tout simplement si les deux
     * intervalles s'intersectent on ajoute la référence. On n'effectue pas ces
     * comparaisons sur tous les éléments de l'arbre, c'est là qu'intervient
     * dmax... (expliqué plus bas)
     *
     * @param date donnée indiquée par l'utilisateur qui fixe la date de
     * recherche par rapport aux dates des événements présents dans l'arbre
     * @return l'ensemble des événements qui intersectent cette date
     */
    public ArrayList<String> searchEvents(String date) {
        ArrayList<String> res = new ArrayList<>(); //variable qui stockera les événements à retourner
        Interval i = Interval.DateToInterval(date); //On convertit la date en intervalle (méthode statique de Interval)
        if (this.node.Intersect(i)) {
            res.addAll(this.eventAssociated); //si l'intervalle de this intersecte la date de recherche on ajoute l'événement 
        }
        if (!(droite == null)) {
            //si tous les intervalles présents dans le sous-arbre droit ont une borne droite inférieure 
            //à la borne gauche de l'intervalle de recherche, alors jamais ces derniers ne s'intersecteront
            //ici la condition est l'inverse du prédicat ci-dessus
            if (!(droite.dmax < i.getLow())) {
                res.addAll(droite.searchEvents(date));
            }
        }
        if (!(gauche == null)) {
            //si tous les intervalles présents dans le sous-arbre gauche ont une borne droite inférieure 
            //à la borne gauche de l'intervalle de recherche, alors jamais ces derniers ne s'intersecteront
            //ici la condition est l'inverse du prédicat ci-dessus
            if (!(gauche.dmax < i.getLow())) {
                res.addAll(gauche.searchEvents(date));
            }
        }
        return res;
    }

}
