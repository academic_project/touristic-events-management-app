/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dateSearch;

import exceptionPersonnalise.IntervallFormatException;
import eventGestion.Event;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cette classe modélise un intervalle. Un intervalle possède une référence pour
 * faire le lien entre un événement et un intervalle
 *
 * @author loic and yacouba
 */
public class Interval {

    private int low, high;
    private String ref;

    public final static Interval INTERVALLE_VIDE = new Interval();

    /**
     * construit l'intervalle nul utile pour contruire un arbre vide
     */
    public Interval() {
        this.low = 0;
        this.high = 0;
        this.ref = null;
    }

    /**
     * construit un intervalle dont on ne sait pas encore la référence associée
     *
     * @param low
     * @param high
     * @throws exceptionPersonnalise.IntervallFormatException
     */
    public Interval(int low, int high) throws IntervallFormatException {
        this(low, high, null);
    }

    /**
     * construit un intervalle ( le cas où high=low correspond à un singleton)
     * dont on vérifie la cohérence des valeurs passés
     *
     * @param low
     * @param high
     * @param ref
     * @throws exceptionPersonnalise.IntervallFormatException
     */
    public Interval(int low, int high, String ref) throws IntervallFormatException {
        // les valeurs passées en paramètres doivent être croissantes entre elles, 
        // et supérieures ou égales à 1 car le jour 0 n'existe pas dans une année
        // et inférieures à 366 
        if (high < low || low <= 0 || high <= 0 || high > 366 || low > 366) {
            throw new IntervallFormatException(low, high);
        } else {
            this.low = low;
            this.high = high;
            this.ref = ref;
        }
    }

    @Override
    public String toString() {
        return "[" + low + "," + high + "] (ref=" + ref + ")";
    }

    /**
     * getter de la borne inférieure de l'intervalle
     *
     * @return borne gauche
     */
    public int getLow() {
        return low;
    }

    /**
     * getter de la borne supérieure de l'intervalle
     *
     * @return borne droite
     */
    public int getHigh() {
        return high;
    }

    /**
     * getter de la référence associée à l'intervalle
     *
     * @return une chaîne de caractères correspondant à la référence
     */
    public String getRef() {
        return ref;
    }

    /**
     * On fixe l'intervalle I (car l'intervalle d'un événement est à priori
     * fixé) et on regarde si l'intervalle this chevauche l'intervalle I;
     *
     * Première possibilité: une intersection par la gauche (par rapport à I)->
     * ceci est représenté par la première condition
     *
     * Deuxième possibilité: une intersection par la droite (par rapport à I)->
     * ceci est représenté par la deuxième condition
     *
     * @param I un intervalle fixée au préalable
     * @return true si les deux intervalles s'intersectent, faux sinon
     */
    public boolean Intersect(Interval I) {
        if (I.low <= this.high && this.high <= I.high) {
            return true;
        }
        return I.low <= this.low && this.low <= I.high;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Interval other = (Interval) obj;
        if (this.low != other.low) {
            return false;
        }
        if (this.high != other.high) {
            return false;
        }
        return Objects.equals(this.ref, other.ref);
    }

    /**
     * Créer un objet intervalle à partir d'un objet Event
     *
     * @param e événement à convertir en intervalle
     * @return l'intervalle cohérent à l'événement passé en paramètre
     */
    public static Interval EventToInterval(Event e) {
        try {
            return new Interval(e.getDebut(), e.getFin(), e.getReference());
        } catch (IntervallFormatException ex) {
            Logger.getLogger(Interval.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Précondition : la paramètre date a le bon format d'une date établie dans
     * la classe Event Créer un objet intervalle à partir d'une date
     *
     * @param date la chaine de caractère dont on va extraire les valeurs
     * numériques
     * @return l'objet intervalle (avec référence nulle) en adéquation avec la
     * date passée en paramètre
     */
    public static Interval DateToInterval(String date) {
        try {
            String[] token = date.replaceAll("(\\[|\\])", "").split(",");
            return new Interval(Integer.parseInt(token[0]), Integer.parseInt(token[1]));
        } catch (IntervallFormatException ex) {
            Logger.getLogger(Interval.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
